package chaincode

import (
	"encoding/json"
	"fmt"
	"inlecom/iw-net/chaincode/model"
	"strings"

	"github.com/hyperledger/fabric-contract-api-go/contractapi"
)

const (
	eventGsinCompKey string = "gsin~eventId"
	eventSsccCompKey string = "sscc~eventId"
	tsnGsinCompKey   string = "gsin~tsnId"
)

func (s *SmartContract) CreateEvent(
	ctx contractapi.TransactionContextInterface,
	id, sscc, gsin, timestamp, eme, hashEvent, hashRecord, hardDelivery string) error {
	exists, err := s.AssetExists(ctx, id)
	if err != nil {
		return err
	}
	if exists {
		return fmt.Errorf("the asset %s (Event) already exists", id)
	}

	event := model.Event{
		Id:           id,
		Sscc:         sscc,
		Gsin:         gsin,
		Timestamp:    timestamp,
		Eme:          eme,
		HashEvent:    hashEvent,
		HashRecord:   hashRecord,
		HashDelivery: hardDelivery,
	}

	err = s.putTransportEvent(ctx, event, "CreateEvent")
	if err != nil {
		return err
	}
	return nil
}

func (s *SmartContract) GetEvent(ctx contractapi.TransactionContextInterface, id string) (*model.Event, error) {
	eventJSON, err := ctx.GetStub().GetState(id)
	if err != nil {
		return nil, fmt.Errorf("failed to read from world state: %v", err)
	}
	if eventJSON == nil {
		return nil, fmt.Errorf("the event %s does not exist", id)
	}

	var event model.Event
	err = json.Unmarshal(eventJSON, &event)
	if err != nil {
		return nil, err
	}

	return &event, nil
}

func (s *SmartContract) CreateTsn(
	ctx contractapi.TransactionContextInterface,
	tsnId, legId, gsin, eme, hashTsn, code string) error {

	tsn := model.Tsn{
		TsnId:   tsnId,
		LegId:   legId,
		Gsin:    gsin,
		Eme:     eme,
		HashTsn: hashTsn,
		Code:    code,
	}

	err := s.putTsn(ctx, tsn, "CreateTsn")
	if err != nil {
		return err
	}

	//update transport instruction
	err = s.updateTranspInstrCodes(ctx, tsn.LegId, tsn.Code)
	if err != nil {
		return err
	}
	return nil
}

func (s *SmartContract) putTransportEvent(ctx contractapi.TransactionContextInterface, event model.Event,
	eventName string) error {
	eventJSON, err := json.Marshal(event)
	if err != nil {
		return err
	}

	err = ctx.GetStub().PutState(event.Id, eventJSON)
	if err != nil {
		return err
	}

	//create and save composite key for GSIN
	err = createAndSaveCompositeKey(ctx, eventGsinCompKey, event.Gsin, event.Id)
	if err != nil {
		return err
	}

	//create and save composite key for SSCC
	err = createAndSaveCompositeKey(ctx, eventSsccCompKey, event.Sscc, event.Id)
	if err != nil {
		return err
	}

	err = ctx.GetStub().SetEvent(eventName, eventJSON)
	if err != nil {
		return fmt.Errorf("failed to set event: %v", err)
	}

	return nil
}

func (s *SmartContract) putTsn(ctx contractapi.TransactionContextInterface, tsn model.Tsn,
	eventName string) error {
	tsnJson, err := json.Marshal(tsn)
	if err != nil {
		return err
	}

	err = ctx.GetStub().PutState(tsn.TsnId, tsnJson)
	if err != nil {
		return err
	}

	gsinTsnIndexKey, err := ctx.GetStub().CreateCompositeKey(tsnGsinCompKey, []string{tsn.Gsin, tsn.TsnId})
	if err != nil {
		return err
	}
	//  Save index entry to world state. Only the key name is needed, no need to store a duplicate copy of the asset.
	//  Note - passing a 'nil' value will effectively delete the key from state, therefore we pass null character as value
	value := []byte{0x00}
	err = ctx.GetStub().PutState(gsinTsnIndexKey, value)
	if err != nil {
		return err
	}

	err = ctx.GetStub().SetEvent(eventName, tsnJson)
	if err != nil {
		return fmt.Errorf("failed to set event: %v", err)
	}

	return nil
}

func createAndSaveCompositeKey(ctx contractapi.TransactionContextInterface, objectType, attr1, attr2 string) error {
	eventIndexKey, err := ctx.GetStub().CreateCompositeKey(objectType, []string{attr1, attr2})
	if err != nil {
		return err
	}
	//  Save index entry to world state. Only the key name is needed, no need to store a duplicate copy of the asset.
	//  Note - passing a 'nil' value will effectively delete the key from state, therefore we pass null character as value
	value := []byte{0x00}
	err = ctx.GetStub().PutState(eventIndexKey, value)
	if err != nil {
		return err
	}
	return nil
}

func (s *SmartContract) updateTranspInstrCodes(ctx contractapi.TransactionContextInterface, legId, code string) error {
	if code == "" {
		return nil
	}

	// get transport instruction
	transpInstr, err := s.getTransportInstruction(ctx, legId)
	if err != nil {
		return err
	}

	noSpaceCode := strings.ReplaceAll(code, " ", "")
	codes := strings.Split(noSpaceCode, ",")

	const yes = "yes"
	for _, code := range codes {
		switch code {
		case "20":
			transpInstr.Delay = yes
		case "18":
			transpInstr.Damaged = yes
		case "87E":
			transpInstr.WrongRoute = yes
		case "1E":
			transpInstr.Arrived = yes
		case "100":
			transpInstr.Transshipment = yes
		case "29":
			transpInstr.Unload = yes
		case "21":
			transpInstr.Dropoff = yes
		case "31":
			transpInstr.Enroute = yes
		case "24":
			transpInstr.Departure = yes
		case "13":
			transpInstr.Pickup = yes
		case "67":
			transpInstr.Positioned = yes
		case "71b":
			transpInstr.Readyfortransport = yes
		}
	}

	err = s.UpdateTransportInstruction(ctx, transpInstr.LegId, transpInstr.Gsin, transpInstr.Ginc, transpInstr.Eme,
		transpInstr.HashTi, transpInstr.Status, transpInstr.Delay, transpInstr.Damaged, transpInstr.WrongRoute,
		transpInstr.Arrived, transpInstr.Transshipment, transpInstr.Unload, transpInstr.Dropoff, transpInstr.Enroute,
		transpInstr.Departure, transpInstr.Pickup, transpInstr.Positioned, transpInstr.Readyfortransport)
	return err
}
