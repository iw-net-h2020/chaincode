package chaincode

import (
	"encoding/json"
	"fmt"
	"inlecom/iw-net/chaincode/model"

	"github.com/hyperledger/fabric-contract-api-go/contractapi"
)

const (
	transpInstrCompKey     = "gsin~transpInstrLegId"
	transpInstrRespCompKey = "gsin~transpInstrRespId"
)

func (s *SmartContract) CreateTransportInstruction(
	ctx contractapi.TransactionContextInterface,
	legId, gsin, ginc, eme, hashTi, status, delay, damaged, wrongRoute, arrived, transshipment, unload,
	dropoff, enroute, departure, pickup, positioned, readyfortransport string) error {
	exists, err := s.AssetExists(ctx, legId)
	if err != nil {
		return err
	}
	if exists {
		return fmt.Errorf("the asset %s (Transport Instruction) already exists", legId)
	}

	tranpInstr := model.Instruction{
		LegId:             legId,
		Gsin:              gsin,
		Ginc:              ginc,
		Eme:               eme,
		HashTi:            hashTi,
		Status:            status,
		Delay:             delay,
		Damaged:           damaged,
		WrongRoute:        wrongRoute,
		Arrived:           arrived,
		Transshipment:     transshipment,
		Unload:            unload,
		Dropoff:           dropoff,
		Enroute:           enroute,
		Departure:         departure,
		Pickup:            pickup,
		Positioned:        positioned,
		Readyfortransport: readyfortransport,
	}

	err = s.putTransportInstruction(ctx, tranpInstr, "CreateTransportInstruction")
	if err != nil {
		return err
	}
	return nil
}

func (s *SmartContract) UpdateTransportInstruction(
	ctx contractapi.TransactionContextInterface,
	legId, gsin, ginc, eme, hashTi, status, delay, damaged, wrongRoute, arrived, transshipment, unload,
	dropoff, enroute, departure, pickup, positioned, readyfortransport string) error {
	// get transport instruction
	oldTranspInstr, err := s.getTransportInstruction(ctx, legId)
	if err != nil {
		return err
	}

	// get and delete its composite key
	oldTranspInstrIndexKey, err := ctx.GetStub().CreateCompositeKey(transpInstrCompKey, []string{oldTranspInstr.Gsin, oldTranspInstr.LegId})
	if err != nil {
		return err
	}
	err = ctx.GetStub().DelState(oldTranspInstrIndexKey)
	if err != nil {
		return err
	}

	tranpInstr := model.Instruction{
		LegId:             legId,
		Gsin:              gsin,
		Ginc:              ginc,
		Eme:               eme,
		HashTi:            hashTi,
		Status:            status,
		Delay:             delay,
		Damaged:           damaged,
		WrongRoute:        wrongRoute,
		Arrived:           arrived,
		Transshipment:     transshipment,
		Unload:            unload,
		Dropoff:           dropoff,
		Enroute:           enroute,
		Departure:         departure,
		Pickup:            pickup,
		Positioned:        positioned,
		Readyfortransport: readyfortransport,
	}

	err = s.putTransportInstruction(ctx, tranpInstr, "UpdateTransportInstruction")
	if err != nil {
		return err
	}
	return nil
}

func (s *SmartContract) CreateTransportInstructionResponse(
	ctx contractapi.TransactionContextInterface,
	instrRespId, legId, gsin, eme, hashTir, instrStatus string) error {

	tranpInstrResp := model.TransInstructionResp{
		InstrRespId: instrRespId,
		LegId:       legId,
		Gsin:        gsin,
		Eme:         eme,
		HashTir:     hashTir,
	}

	err := s.putTransportInstructionResp(ctx, tranpInstrResp, "CreateTransportInstruction")
	if err != nil {
		return err
	}

	err = s.updateTranspInstrStatus(ctx, legId, instrStatus)
	if err != nil {
		return err
	}

	return nil
}

func (s *SmartContract) getTransportInstruction(ctx contractapi.TransactionContextInterface, id string) (*model.Instruction, error) {
	instrJSON, err := ctx.GetStub().GetState(id)
	if err != nil {
		return nil, fmt.Errorf("failed to read from world state: %v", err)
	}
	if instrJSON == nil {
		return nil, fmt.Errorf("the transport instruction %s does not exist", id)
	}

	var instr model.Instruction
	err = json.Unmarshal(instrJSON, &instr)
	if err != nil {
		return nil, err
	}

	return &instr, nil
}

func (s *SmartContract) putTransportInstruction(ctx contractapi.TransactionContextInterface, tranpInstr model.Instruction,
	eventName string) error {
	tranpInstrJSON, err := json.Marshal(tranpInstr)
	if err != nil {
		return err
	}

	err = ctx.GetStub().PutState(tranpInstr.LegId, tranpInstrJSON)
	if err != nil {
		return err
	}

	gsinInstrIndexKey, err := ctx.GetStub().CreateCompositeKey(transpInstrCompKey, []string{tranpInstr.Gsin, tranpInstr.LegId})
	if err != nil {
		return err
	}
	//  Save index entry to world state. Only the key name is needed, no need to store a duplicate copy of the asset.
	//  Note - passing a 'nil' value will effectively delete the key from state, therefore we pass null character as value
	value := []byte{0x00}
	err = ctx.GetStub().PutState(gsinInstrIndexKey, value)
	if err != nil {
		return err
	}

	err = ctx.GetStub().SetEvent(eventName, tranpInstrJSON)
	if err != nil {
		return fmt.Errorf("failed to set event: %v", err)
	}

	return nil
}

func (s *SmartContract) putTransportInstructionResp(ctx contractapi.TransactionContextInterface, tranpInstrResp model.TransInstructionResp,
	eventName string) error {
	tranpInstrRespJSON, err := json.Marshal(tranpInstrResp)
	if err != nil {
		return err
	}

	err = ctx.GetStub().PutState(tranpInstrResp.InstrRespId, tranpInstrRespJSON)
	if err != nil {
		return err
	}

	gsinInstrRespIndexKey, err := ctx.GetStub().CreateCompositeKey(transpInstrRespCompKey, []string{tranpInstrResp.Gsin, tranpInstrResp.InstrRespId})
	if err != nil {
		return err
	}
	//  Save index entry to world state. Only the key name is needed, no need to store a duplicate copy of the asset.
	//  Note - passing a 'nil' value will effectively delete the key from state, therefore we pass null character as value
	value := []byte{0x00}
	err = ctx.GetStub().PutState(gsinInstrRespIndexKey, value)
	if err != nil {
		return err
	}

	err = ctx.GetStub().SetEvent(eventName, tranpInstrRespJSON)
	if err != nil {
		return fmt.Errorf("failed to set event: %v", err)
	}

	return nil
}

func (s *SmartContract) updateTranspInstrStatus(ctx contractapi.TransactionContextInterface, legId, status string) error {
	if status == "" {
		return nil
	}

	// get transport instruction
	transpInstr, err := s.getTransportInstruction(ctx, legId)
	if err != nil {
		return err
	}

	err = s.UpdateTransportInstruction(ctx, transpInstr.LegId, transpInstr.Gsin, transpInstr.Ginc, transpInstr.Eme,
		transpInstr.HashTi, status, transpInstr.Delay, transpInstr.Damaged, transpInstr.WrongRoute,
		transpInstr.Arrived, transpInstr.Transshipment, transpInstr.Unload, transpInstr.Dropoff, transpInstr.Enroute,
		transpInstr.Departure, transpInstr.Pickup, transpInstr.Positioned, transpInstr.Readyfortransport)
	return err
}
