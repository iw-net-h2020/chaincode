package chaincode

import (
	"encoding/json"
	"fmt"
	"inlecom/iw-net/chaincode/model"

	"github.com/hyperledger/fabric-contract-api-go/contractapi"
)

func (s *SmartContract) GetEventsBySscc(ctx contractapi.TransactionContextInterface, sscc string) ([]*model.Event, error) {
	// range query with empty string for startKey and endKey does an
	// open-ended query of all assets in the chaincode namespace.
	eventsResultsIterator, err := ctx.GetStub().GetStateByPartialCompositeKey(eventSsccCompKey, []string{sscc})
	if err != nil {
		return nil, err
	}
	defer eventsResultsIterator.Close()

	var events []*model.Event
	for eventsResultsIterator.HasNext() {
		responseRange, err := eventsResultsIterator.Next()
		if err != nil {
			return nil, err
		}

		_, compositeKeyParts, err := ctx.GetStub().SplitCompositeKey(responseRange.Key)
		if err != nil {
			return nil, err
		}

		if len(compositeKeyParts) > 1 {
			returnedEventId := compositeKeyParts[1]
			event, err := s.GetEvent(ctx, returnedEventId)
			if err != nil {
				return nil, err
			}
			events = append(events, event)
		}
	}
	return events, nil
}

func (s *SmartContract) GetRecordsByGsin(ctx contractapi.TransactionContextInterface, gsin string) ([]interface{}, error) {
	instructions, err := s.getTransportInstrByGsin(ctx, gsin)
	if err != nil {
		return nil, err
	}

	tsns, err := s.getTsnByGsin(ctx, gsin)
	if err != nil {
		return nil, err
	}

	tirs, err := s.getTirByGsin(ctx, gsin)
	if err != nil {
		return nil, err
	}

	totalLength := len(instructions) + len(tsns) + len(tirs)
	resultingSlice := make([]interface{}, totalLength)
	i := 0
	for _, instr := range instructions {
		resultingSlice[i] = instr
		i = i + 1
	}
	for _, tsn := range tsns {
		resultingSlice[i] = tsn
		i = i + 1
	}
	for _, tir := range tirs {
		resultingSlice[i] = tir
		i = i + 1
	}

	return resultingSlice, nil
}

func (s *SmartContract) getTransportInstrByGsin(ctx contractapi.TransactionContextInterface, gsin string) ([]*model.Instruction, error) {
	// range query with empty string for startKey and endKey does an
	// open-ended query of all assets in the chaincode namespace.
	instructionResultsIterator, err := ctx.GetStub().GetStateByPartialCompositeKey(transpInstrCompKey, []string{gsin})
	if err != nil {
		return nil, err
	}
	defer instructionResultsIterator.Close()

	var instructions []*model.Instruction
	for instructionResultsIterator.HasNext() {
		responseRange, err := instructionResultsIterator.Next()
		if err != nil {
			return nil, err
		}

		_, compositeKeyParts, err := ctx.GetStub().SplitCompositeKey(responseRange.Key)
		if err != nil {
			return nil, err
		}

		if len(compositeKeyParts) > 1 {
			returnedInstructionId := compositeKeyParts[1]
			instruction, err := s.getTransportInstruction(ctx, returnedInstructionId)
			if err != nil {
				return nil, err
			}
			instructions = append(instructions, instruction)
		}
	}
	return instructions, nil
}

func (s *SmartContract) getTsnByGsin(ctx contractapi.TransactionContextInterface, gsin string) ([]*model.Tsn, error) {
	// range query with empty string for startKey and endKey does an
	// open-ended query of all assets in the chaincode namespace.
	tsnResultsIterator, err := ctx.GetStub().GetStateByPartialCompositeKey(tsnGsinCompKey, []string{gsin})
	if err != nil {
		return nil, err
	}
	defer tsnResultsIterator.Close()

	var tsns []*model.Tsn
	for tsnResultsIterator.HasNext() {
		responseRange, err := tsnResultsIterator.Next()
		if err != nil {
			return nil, err
		}

		_, compositeKeyParts, err := ctx.GetStub().SplitCompositeKey(responseRange.Key)
		if err != nil {
			return nil, err
		}

		if len(compositeKeyParts) > 1 {
			returnedTsnId := compositeKeyParts[1]
			tsn, err := s.getTsn(ctx, returnedTsnId)
			if err != nil {
				return nil, err
			}
			tsns = append(tsns, tsn)
		}
	}
	return tsns, nil
}

func (s *SmartContract) getTsn(ctx contractapi.TransactionContextInterface, legId string) (*model.Tsn, error) {
	tsnJSON, err := ctx.GetStub().GetState(legId)
	if err != nil {
		return nil, fmt.Errorf("failed to read from world state: %v", err)
	}
	if tsnJSON == nil {
		return nil, fmt.Errorf("the Tsn %s does not exist", legId)
	}

	var tsn model.Tsn
	err = json.Unmarshal(tsnJSON, &tsn)
	if err != nil {
		return nil, err
	}

	return &tsn, nil
}

func (s *SmartContract) getTirByGsin(ctx contractapi.TransactionContextInterface, gsin string) ([]*model.TransInstructionResp, error) {
	// range query with empty string for startKey and endKey does an
	// open-ended query of all assets in the chaincode namespace.
	tirResultsIterator, err := ctx.GetStub().GetStateByPartialCompositeKey(transpInstrRespCompKey, []string{gsin})
	if err != nil {
		return nil, err
	}
	defer tirResultsIterator.Close()

	var tirs []*model.TransInstructionResp
	for tirResultsIterator.HasNext() {
		responseRange, err := tirResultsIterator.Next()
		if err != nil {
			return nil, err
		}

		_, compositeKeyParts, err := ctx.GetStub().SplitCompositeKey(responseRange.Key)
		if err != nil {
			return nil, err
		}

		if len(compositeKeyParts) > 1 {
			returnedTirId := compositeKeyParts[1]
			tir, err := s.getTir(ctx, returnedTirId)
			if err != nil {
				return nil, err
			}
			tirs = append(tirs, tir)
		}
	}
	return tirs, nil
}

func (s *SmartContract) getTir(ctx contractapi.TransactionContextInterface, legId string) (*model.TransInstructionResp, error) {
	tirJSON, err := ctx.GetStub().GetState(legId)
	if err != nil {
		return nil, fmt.Errorf("failed to read from world state: %v", err)
	}
	if tirJSON == nil {
		return nil, fmt.Errorf("the Tir %s does not exist", legId)
	}

	var tir model.TransInstructionResp
	err = json.Unmarshal(tirJSON, &tir)
	if err != nil {
		return nil, err
	}

	return &tir, nil
}
