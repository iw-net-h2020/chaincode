package model

//Transport instruction
type Instruction struct {
	LegId             string `json:"legId"`
	Gsin              string `json:"gsin"`
	Ginc              string `json:"ginc"`
	Eme               string `json:"eme"`
	HashTi            string `json:"hash-ti"`
	Status            string `json:"status"`
	Delay             string `json:"delay"`
	Damaged           string `json:"damaged"`
	WrongRoute        string `json:"wrongroute"`
	Arrived           string `json:"arrived"`
	Transshipment     string `json:"transshipment"`
	Unload            string `json:"unload"`
	Dropoff           string `json:"dropoff"`
	Enroute           string `json:"enroute"`
	Departure         string `json:"departure"`
	Pickup            string `json:"pickup"`
	Positioned        string `json:"positioned"`
	Readyfortransport string `json:"readyfortransport"`
}
