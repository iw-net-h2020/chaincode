package model

//Transport instruction responce
type TransInstructionResp struct {
	InstrRespId string `json:"instrRespId"`
	LegId       string `json:"legId"`
	Gsin        string `json:"gsin"`
	Eme         string `json:"eme"`
	HashTir     string `json:"hash-tir"`
}
