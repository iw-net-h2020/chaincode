package model

//Event
type Event struct {
	Id           string `json:"id"`
	Sscc         string `json:"sscc"`
	Gsin         string `json:"gsin"`
	Timestamp    string `json:"timestamp"`
	Eme          string `json:"eme"`
	HashEvent    string `json:"hash-event"`
	HashRecord   string `json:"hash-record"`
	HashDelivery string `json:"hash-delivery"`
}
