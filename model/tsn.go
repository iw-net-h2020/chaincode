package model

//Transport Status Notification
type Tsn struct {
	TsnId   string `json:"tsnId"`
	LegId   string `json:"legId"`
	Gsin    string `json:"gsin"`
	Eme     string `json:"eme"`
	HashTsn string `json:"hash-tsn"`
	Code    string `json:"code"`
}
